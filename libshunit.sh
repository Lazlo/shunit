#!/bin/bash

# Protect library from being sourced more than once
if [[ ${LIBSHUNIT_SOURCED:-} -eq 1 ]]; then return; else readonly LIBSHUNIT_SOURCED=1; fi

set -e
set -u

declare -r -i LIBSHUNIT_VERSION_MAJOR=1
declare -r -i LIBSHUNIT_VERSION_MINOR=0
declare -r -i LIBSHUNIT_VERSION_PATCH=73

declare -r LIBSHUNIT_VERSION="${LIBSHUNIT_VERSION_MAJOR}.${LIBSHUNIT_VERSION_MINOR}.${LIBSHUNIT_VERSION_PATCH}"

#
# Misc
#

shunit_version() {
	echo "$LIBSHUNIT_VERSION"
}

generate_testcase() {
	local -r tc_subject="$1"
	echo "#!/bin/bash\n\nset -e\nset -u\n\nsource libshunit.sh\n\ntestcase_${tc_subject}() {\n\n\ttestcase_${tc_subject}_setup() {\n\t\treturn\n\t}\n\n\ttestcase_${tc_subject}_teardown() {\n\t\treturn\n\t}\n\n\ttest_${tc_subject}_placeholder() {\n\t\texpected=\"0\"\n\t\tactual=\"1\"\n\t\tassertEquals \"\$expected\" \"\$actual\"\n\t}\n}"
}

verbose_puts() {
	local -r -i ctx="$1"
	if [[ "$(formatter_opt_verbose_get $ctx)" = "0" ]]; then
		return
	fi
	local -r str="$2"
	echo -e -n "$str"
}

verbose_putln() {
	local -r -i ctx="$1"
	local -r msg="$2"
	verbose_puts $ctx "$msg\n"
}

#
# Locator
#

__libshunit_testcase_prefix="testcase_"
__libshunit_testcase_file_ext="sh"

locator_find_testcase_files() {
	local where="$1"
	local tc_prefix="$__libshunit_testcase_prefix"
	local res=""
	for ent in $(ls -1 $where); do
		# Skip directories
		if [ -d $where/$ent ]; then
			continue
		fi
		# Check if file matches naming pattern
		set +e
		echo $ent | grep "^$tc_prefix" > /dev/null
		local rc=$?
		set -e
		if [ "$rc" != "0" ]; then
			continue
		fi
		res="$res $ent"
	done
	echo $res
}

__libshunit_testcase_file_to_name() {
	local tc_file="$1"
	local tc_file_ext="$__libshunit_testcase_file_ext"
	local tc_name="$(basename $tc_file | sed "s/\.${tc_file_ext}$//")"
	echo $tc_name
}

__libshunit_testcase_name_to_file() {
	local tc_name="$1"
	local tc_file_ext="$__libshunit_testcase_file_ext"
	local tc_file="${tc_name}.${tc_file_ext}"
	echo $tc_file
}

__libshunit_testcase_name_to_subject() {
	local tc_name="$1"
	local tc_prefix="$__libshunit_testcase_prefix"
	local tc_subject="$(echo $tc_name | sed "s/${tc_prefix}//")"
	echo $tc_subject
}

locator_load_testcase() {
	local tc_file="$1"
	local tc_name="$(__libshunit_testcase_file_to_name $tc_file)"
	# Source the script to have top-level testcase function declared
	source $tc_file
	# Call testcase function to declare its setup, teardown and unit test functions
	$tc_name
}

__libshunit_func_exists() {
	local func_name="$1"
	local rc
	set +e
	declare -F | grep "^declare -f ${func_name}$" > /dev/null
	local rc=$?
	set -e
	if [ "$rc" = "0" ]; then
		echo 0 # true
	else
		echo 1 # false
	fi
}

locator_testcase_has_setup() {
	local tc_file="$1"
	local tc_name="$(__libshunit_testcase_file_to_name $tc_file)"
	local tc_setup_func_name="${tc_name}_setup"
	echo $(__libshunit_func_exists $tc_setup_func_name)
}

locator_testcase_has_teardown() {
	local tc_file="$1"
	local tc_name="$(__libshunit_testcase_file_to_name $tc_file)"
	local tc_teardown_func_name="${tc_name}_teardown"
	echo $(__libshunit_func_exists $tc_teardown_func_name)
}

locator_find_unittests_by_testcase_name() {
	local where="$1"
	local tc_name="$2"
	local tc_file="$(__libshunit_testcase_name_to_file $tc_name)"
	local tc_prefix="$__libshunit_testcase_prefix"
	local ut_prefix="$(echo $tc_name | sed "s/^${tc_prefix}/test_/")"
	source $where/$tc_file
	echo $(declare -F | grep "declare -f $ut_prefix" | sed 's/declare -f //' | sed 's/\r//')
}

#
# Assert
#

assertEquals() {
	local expected="$1"
	local actual="$2"
	if [ "$expected" = "$actual" ]; then
		echo 0
	else
		echo 1
		local -r verbose=$__libshunit_formatter_opt_verbose
		if [ "$verbose" = "1" ]; then
			>&2 echo -e "\n\texpected: $expected\n\tactual: $actual"
		fi
	fi
}

#
# Formatter
#

__libshunit_fail_string_short="!"
__libshunit_fail_string_long="failed!"
__libshunit_pass_string_short="."
__libshunit_pass_string_long="passed"

fail() {
	local verbose=$__libshunit_formatter_opt_verbose
	local -r short="$__libshunit_fail_string_short"
	local -r long="$__libshunit_fail_string_long"
	if [ "$verbose" = "0" ]; then
		echo "$short"
	else
		echo "$long"
	fi
}

pass() {
	local verbose=$__libshunit_formatter_opt_verbose
	local -r short="$__libshunit_pass_string_short"
	local -r long="$__libshunit_pass_string_long"
	if [ "$verbose" = "0" ]; then
		echo "$short"
	else
		echo "$long"
	fi
}

colorize() {
	local -r input_str="$1"
	local -r color="$2"
	local -r color_code_none="\e[0m"
	case $color in
	"green")
		color_code="\e[32m"
		;;
	"red")
		color_code="\e[31m"
		;;
	# black 30
	# orange 33
	# blue 34
	# purple 35
	# mint 36
	# white 37
	esac
	local -r output_str="${color_code}${input_str}${color_code_none}"
	echo $output_str
}

__libshunit_formatter_opt_verbose=0

formatter_opt_verbose_get() {
	local ctx="$1"
	echo $__libshunit_formatter_opt_verbose
}

formatter_opt_verbose_set() {
	local ctx="$1"
	local verbose="$2"
	__libshunit_formatter_opt_verbose=$verbose
}

#
# Report
#

declare -a __libshunit_report_stats_tests_total=()
declare -a __libshunit_report_stats_tests_failed=()
declare -a __libshunit_report_stats_tests_passed=()

report_stats_reset() {
	local ctx="$1"
	__libshunit_report_stats_tests_total[$ctx]=0
	__libshunit_report_stats_tests_failed[$ctx]=0
	__libshunit_report_stats_tests_passed[$ctx]=0
}

__report_stats_get() {
	local -r -i ctx="$1"
	local -r kind="$2"
	eval "echo \${__libshunit_report_stats_$kind[$ctx]}"
}

report_stats_tests_total_get() {
	local ctx="$1"
	local -r kind="tests_total"
	__report_stats_get $ctx $kind
}

report_stats_tests_failed_get() {
	local ctx="$1"
	local -r kind="tests_failed"
	__report_stats_get $ctx $kind
}

report_stats_tests_passed_get() {
	local ctx="$1"
	local -r kind="tests_passed"
	__report_stats_get $ctx $kind
}

report_stats_tests_total_inc() {
	local ctx="$1"
	local -r kind="tests_total"
	local -r -i n=$(__report_stats_get $ctx $kind)
	__libshunit_report_stats_tests_total[$ctx]=$(($n + 1))
}

report_stats_tests_failed_inc() {
	local ctx="$1"
	local -r kind="tests_failed"
	local -r -i n=$(__report_stats_get $ctx $kind)
	__libshunit_report_stats_tests_failed[$ctx]=$(($n + 1))
}

report_stats_tests_passed_inc() {
	local ctx="$1"
	local -r kind="tests_passed"
	local -r -i n=$(__report_stats_get $ctx $kind)
	__libshunit_report_stats_tests_passed[$ctx]=$(($n + 1))
}

report_print_oneline() {
	local ctx="$1"
	local -r -i cnt_total=$(report_stats_tests_total_get $ctx)
	local -r -i cnt_passed=$(report_stats_tests_passed_get $ctx)
	local -r -i cnt_failed=$(report_stats_tests_failed_get $ctx)
	if [[ $cnt_total = $cnt_passed ]]; then
		echo -n "OK"
	else
		echo -n "FAILED"
	fi
	echo -n " ($cnt_total total, "
	echo -n "$cnt_passed passed, "
	echo "$cnt_failed failed, $SECONDS seconds)"
}

report_exitcode() {
	local ctx="$1"
	local -r -i cnt_failed=$(report_stats_tests_failed_get $ctx)
	if [[ $cnt_failed = 0 ]]; then
		echo 0
	else
		echo 1
	fi
}

#
# Runner
#

declare -a __libshunit_context_list=()

runner_create_context() {
	local -r ctx=$RANDOM
	local -r -i i=${#__libshunit_context_list[@]}
	__libshunit_context_list[$i]=$ctx
	report_stats_reset $ctx
}

runner_get_context() {
	local -r -i i=${#__libshunit_context_list[@]}
	echo ${__libshunit_context_list[$(($i - 1))]}
}

runner_call_testcase_setup() {
	local ctx="$1"
	tc_name="$2"
	if [ "$(locator_testcase_has_setup $tc_name)" = "1" ]; then
		return
	fi
	local tc_setup_func_name="${tc_name}_setup"
	$tc_setup_func_name
}

runner_call_testcase_teardown() {
	local ctx="$1"
	tc_name="$2"
	if [ "$(locator_testcase_has_teardown $tc_name)" = "1" ]; then
		return
	fi
	local tc_teardown_func_name="${tc_name}_teardown"
	$tc_teardown_func_name
}

__libshunit_unittest_last_result=999

runner_call_unittest() {
	local ctx="$1"
	local tc_name="$2"
	local ut_fnc_name="$3"
	runner_call_testcase_setup $ctx $tc_name
	report_stats_tests_total_inc $ctx
	local rc=$($ut_fnc_name)
	if [ "$rc" = "0" ]; then
		report_stats_tests_passed_inc $ctx
	elif [ "$rc" = "1" ]; then
		report_stats_tests_failed_inc $ctx
	else
		>&2 echo -e "\nWARNING: ${FUNCNAME[1]}() did not set rc! Make sure the test calls assertEquals()."
	fi
	__libshunit_unittest_last_result=$rc
	runner_call_testcase_teardown $ctx $tc_name
}

runner_get_unittest_outcome() {
	echo $__libshunit_unittest_last_result
}
