#!/bin/bash

set -e
set -u

source libshunit.sh

create_testcase_file() {
	local -r tc_subject="$1"
	local -r tc_name="testcase_${tc_subject}"
	local -r tc_file="${tc_name}.sh"
	echo -e "$(generate_testcase $tc_subject)" > $tc_file
	chmod +x $tc_file
}

if [[ $# != 1 ]]; then
	echo "USAGE: $0 <testcase-subject>"
	exit 1
fi
create_testcase_file $1
