#!/bin/bash

set -e
set -u

create_deb_control_file() {
	local -r deb_dir="$1"
	local -r pkg_name="$2"
	local -r pkg_desc="$3"
	local -r pkg_maintainer="$4"
	local -r pkg_version="$5"
	local -r pkg_arch="$6"
	f=$deb_dir/control
	> $f
	echo "Package: $pkg_name" >> $f
	echo "Description: $pkg_desc" >> $f
	echo "Maintainer: $pkg_maintainer" >> $f
	echo "Version: $pkg_version" >> $f
	echo "Architecture: $pkg_arch" >> $f
}

main() {
	local -r pkg_dir="tmp"
	local -r deb_dir=$pkg_dir/DEBIAN

	source libshunit.sh

	local -r pkg_name="shunit"
	local -r pkg_desc="FIXME"
	local -r pkg_maintainer="$(cat MAINTAINERS)"
	local -r pkg_version="$(shunit_version)"
	local -r pkg_arch="all"

	local -r deb_file="${pkg_name}_${pkg_version}_${pkg_arch}.deb"

	# Create temporary package directory (and remove it before we create it)
	rm -rf $pkg_dir
	mkdir $pkg_dir

	# Create DEBIAN directory
	mkdir -p $deb_dir
	create_deb_control_file $deb_dir "$pkg_name" "$pkg_desc" "$pkg_maintainer" "$pkg_version" "$pkg_arch"
	touch $deb_dir/preinst
	touch $deb_dir/postinst
	touch $deb_dir/prerm
	touch $deb_dir/postrm
	chmod +x $deb_dir/preinst $deb_dir/postinst $deb_dir/prerm $deb_dir/postrm

	# Put shunit files into package directory
	./scripts/create-package.sh $pkg_dir

	# Build the deb file
	dpkg --build $pkg_dir $deb_file

	# Clean up
	rm -rf $pkg_dir
}

main $@
