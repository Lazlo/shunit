#!/bin/bash

deploy_to_pkg_dir() {
	local -r pkg_dir="$1"
	local -r doc_dir="$pkg_dir/usr/share/doc/shunit"
	local -r share_dir="$pkg_dir/usr/share/shunit"
	local -r bin_dir="$pkg_dir/usr/bin"

	# /usr/share/doc/shunit/
	mkdir -p $doc_dir
	cp README.md MAINTAINERS LICENSE $doc_dir

	# /usr/share/shunit/
	mkdir -p $share_dir
	cp libshunit.sh $share_dir
	cp -rf tests $share_dir

	# /usr/bin/
	mkdir -p $bin_dir
	cp shunit $bin_dir
}

main() {
	pkg_dir="$1"
	deploy_to_pkg_dir $pkg_dir
}

main $@
