# shUnit

Is a xUnit testing framework written for use with UNIX shells.

shUnit is developed in a test-driven way. This allows for verification of the framework itself.

## Features

 * compatible with [Bourne](https://en.wikipedia.org/wiki/Bourne_shell), Bourne-Again (```bash```) and [Almquist](https://en.wikipedia.org/wiki/Almquist_shell) shell (```ash```)
 * automatically detected and executed test cases and unit tests
 * allow specifying the directory where to look for tests (other than the default ```tests```)
 * filter test cases to be executed (single name only)
 * setup and teardown functions of test cases are executed before and after each unit test
 * prints test report with total number of tests, passed, failed and duration in seconds
 * set exit code non-zero when a test fails
 * supports verbose and slient operation (turn on verbose mode using ```-v```)
 * prints expected and actual values of a failed assert (only in verbose mode)
 * provides script to generate testcase file (```scripts/create-testcase.sh```)

## Limitations

 * if a assertion fails, the expected and actual values are not displayed
 * only one assertion per test (which is actually a desirable pattern for tests)

## Usage

### Test Execution

Tests are executed using the ```shunit``` script.

When called without any arguments, it will assume the default settings and look in the current working directory for files matching the test case script naming pattern (```testcase_<subject-name>.sh```).

The script can also be passed command line options

 * ```-c``` to turn off colorization of test results
 * ```-h``` in order to display the help dialog
 * ```-t``` to specify the directory that contains the tests
 * ```-v``` for verbose operation
 * ```-V``` for printing the version

#### Test Case Filter

The script can also be told to execute only a cerain test case and contained unit tests by specifying a test case name filter as an argument.

```
./shunit rocket_engine
```

This example will only execute the test case ```tests/testcase_rocket_engine.sh```.

#### Different Test Directory

The script can also be instructed to look for tests in a different location.

```
./shunit -t .
```

This example will search in the current directory for test case files.

### Test Case Creation

#### Scripted Test Case Creation

Test cases can be created using a script that accepts the test subject name as its only argument.
It wil create a test case file, containing everything needed to start writing your first unit test.

```
./scripts/create-testcase.sh <testcase-subject>
```

#### Manual Test Case Creation

 * create a test case file in ```tests/``` directory, named ```testcase_<name-of-your-software>_<name-of-component-under-test>.sh```
 * make the test script executable using ```chmod +x```
 * add ```source libshunit.sh``` to the top of the new file
 * source the script of your code under test, but be sure to prefix the path with ```$tc_dir``` like ```source $tc_dir/librocket_engine.sh```. NOTE That ```$tc_dir``` should always be used from within the testcase to acces files (like fixtures for example).
 * create new test case function, named ```testcase_<name-of-your-software>_<name-of-component-under-test>```
 * within the test case function, you can optionally add set up and tear down functions. Use the name of the test case function and append ```_setup``` and ```_teardown```
 * add a new function within the test case function, named ```test_<name-of-your-software>_<name-of-component-under-test>_<name-of-unittest>```
 * place a call to ```assertEquals()``` inside the unit test function, while the first argument is the expected, the second is the actual value returned by a function of the component under test
   * it is recommended to use the variable ```expected``` for the expected and ```actual``` for the actual value
   * use these variables in the call to ```assertEquals```
 * on the last line of the test, call ```assertEquals``` like this ```assertEquals "$expected" "$actual"```
 * now you are ready to run ```shunit``` in the top directory of the repository

```
source libshunit.sh
source $tc_dir/librocket_engine.sh

testcase_acme_counter() {

	testcase_acme_counter_setup() {
		return
	}

	testcase_acme_counter_teardown() {
		return
	}

	test_acme_counter_get_returns_zero_on_start() {
		expected="0"
		actual="$(acme_counter_get)"
		assertEquals "$expected" "$actual"
	}
```
Example test case with unit test

## TODO

Ordered by priority:

 * implement assertMatches that can handle regular expressions (uses grep)
 * in silent mode, print list of tests failed at the end
 * make all vars explicitly local that should be local
 * make all vars read-only that are not supposed to changed
 * logging functions
 * logging test results to file
 * in silent mode, print only 50 test results per line
 * have ```locator_load_testcase()``` check if the test case function actually exists (the function that wraps all the unit tests of the test case)
 * prefix "private" vars and functions with ```__```
 * have tests for all functions of ```shunit```
 * move ```shunit``` functions to ```libshunit.sh```
 * exit with error if test case filter did not match anything
 * make ```shunit``` do the work of ```scripts/create-testcase.sh```
