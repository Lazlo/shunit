#!/bin/bash

set -e
set -u

source libshunit.sh

testcase_shunit_misc() {

	test_shunit_misc_get_version() {
		expected="${LIBSHUNIT_VERSION_MAJOR}.${LIBSHUNIT_VERSION_MINOR}.${LIBSHUNIT_VERSION_PATCH}"
		actual="$(shunit_version)"
		assertEquals "$expected" "$actual"
	}

	test_shunit_misc_generate_testcase() {
		local tc_subject="truck_engine"
		expected="#!/bin/bash\n\nset -e\nset -u\n\nsource libshunit.sh\n\ntestcase_${tc_subject}() {\n\n\ttestcase_${tc_subject}_setup() {\n\t\treturn\n\t}\n\n\ttestcase_${tc_subject}_teardown() {\n\t\treturn\n\t}\n\n\ttest_${tc_subject}_placeholder() {\n\t\texpected=\"0\"\n\t\tactual=\"1\"\n\t\tassertEquals \"\$expected\" \"\$actual\"\n\t}\n}"
		actual="$(generate_testcase $tc_subject)"
		assertEquals "$expected" "$actual"
	}

	test_shunit_misc_verbose_puts_echos_nothing_when_verbose_is_off() {
		runner_create_context
		local -r -i ctx=$(runner_get_context)
		expected=""
		formatter_opt_verbose_set $ctx 0
		actual="$(verbose_puts $ctx "teststring")"
		assertEquals "$expected" "$actual"
	}

	test_shunit_misc_verbose_puts_echos_str_when_verbose_is_on() {
		local -r msg="Just a message from the verbose_puts() function"
		runner_create_context
		local -r -i ctx=$(runner_get_context)
		formatter_opt_verbose_set $ctx 1
		expected="$msg"
		actual="$(verbose_puts $ctx "$msg")"
		assertEquals "$expected" "$actual"
	}
}
