#!/bin/bash

set -e
set -u

source libshunit.sh

testcase_shunit_assert() {
	testcase_shunit_assert_setup() {
		runner_create_context
		ctx=$(runner_get_context)
	}
	testcase_shunit_assert_teardown() {
		return
	}

	#
	# Unit Tests
	#

	test_shunit_assert_returns_zero_on_equal() {
		assertEquals 1 1
	}

	test_shunit_assert_returns_one_on_not_equal() {
		# Redirect stderr to /dev/null - This will make sure, when running the tests,
		# we will not see the expected/actual output that we do not care about in this test.
		local rc=$(assertEquals 1 0 2>/dev/null)
		local result
		if [ "$rc" = "1" ]; then
			result=0
		else
			result=1
		fi
		echo $result
	}

	test_shunit_assert_prints_diff_of_expected_and_actual_to_stderr_when_test_fails() {
		expected="1"
		actual="2"
		local -r verbose_previous=$(formatter_opt_verbose_get $ctx)
		formatter_opt_verbose_set $ctx 1
		# Redirect stdout to /dev/null, only check for stderr output
		actual="$(assertEquals "$expected" "$actual" 2>&1 > /dev/null)"
		# Restore verbose setting
		formatter_opt_verbose_set $ctx $verbose_previous
		expected="
	expected: 1
	actual: 2"
		assertEquals "$expected" "$actual"
	}

	test_shunit_assert_does_not_print_diff_of_expected_and_actual_in_silen_mode() {
		expected="3"
		actual="4"
		local -r verbose_previous=$(formatter_opt_verbose_get $ctx)
		formatter_opt_verbose_set $ctx 0
		# Redirect stdout to /dev/null, only check for stderr output
		actual="$(assertEquals "$expected" "$actual" 2>&1 > /dev/null)"
		# Restore verbose setting
		formatter_opt_verbose_set $ctx $verbose_previous
		expected=""
		assertEquals "$expected" "$actual"
	}
}
