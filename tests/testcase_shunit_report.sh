#!/bin/bash

set -e
set -u

source libshunit.sh

testcase_shunit_report() {

	declare ctx

	testcase_shunit_report_setup() {
		runner_create_context
		ctx=$(runner_get_context)
	}

	testcase_shunit_report_teardown() {
		return
	}

	test_shunit_report_stats_reset_sets_all_values_to_zero() {
		expected="0 0 0"
		report_stats_reset $ctx
		actual="$(report_stats_tests_total_get $ctx) $(report_stats_tests_failed_get $ctx) $(report_stats_tests_passed_get $ctx)"
		assertEquals "$expected" "$actual"
	}

	test_shunit_report_stats_tests_total_get_is_zero_after_reset() {
		expected="0"
		report_stats_reset $ctx
		actual=$(report_stats_tests_total_get $ctx)
		assertEquals "$expected" "$actual"
	}

	test_shunit_report_stats_tests_failed_get_is_zero_after_reset() {
		expected="0"
		report_stats_reset $ctx
		actual=$(report_stats_tests_failed_get $ctx)
		assertEquals "$expected" "$actual"
	}

	test_shunit_report_stats_tests_passed_get_is_zero_after_reset() {
		expected="0"
		report_stats_reset $ctx
		actual=$(report_stats_tests_passed_get $ctx)
		assertEquals "$expected" "$actual"
	}

	test_shunit_report_stats_tests_total_inc_adds_one() {
		expected="4"
		report_stats_reset $ctx
		for i in $(seq 4); do
			report_stats_tests_total_inc $ctx
		done
		actual=$(report_stats_tests_total_get $ctx)
		assertEquals "$expected" "$actual"
	}

	test_shunit_report_stats_tests_failed_inc_adds_one() {
		expected="3"
		report_stats_reset $ctx
		for i in $(seq 3); do
			report_stats_tests_failed_inc $ctx
		done
		actual=$(report_stats_tests_failed_get $ctx)
		assertEquals "$expected" "$actual"
	}

	test_shunit_report_stats_tests_passed_inc_adds_one() {
		expected="5"
		report_stats_reset $ctx
		for i in $(seq 5); do
			report_stats_tests_passed_inc $ctx
		done
		actual=$(report_stats_tests_passed_get $ctx)
		assertEquals "$expected" "$actual"
	}

	# NOTE This test fails from time to time becase $SECONDS might change
	# between assigning $expected and $actual.
	# TODO Find a way to make the test pass 100% of the time.
	test_shunit_report_print_oneline_all_passed() {
		local n=15
		# Populate the statistics before we check
		# the one-line report.
		fake_test3() {
			assertEquals "9" "9"
		}
		report_stats_reset $ctx
		for i in $(seq $n); do
			runner_call_unittest $ctx "testcase_nonexistent" "fake_test3"
		done

		expected="OK ($n total, $n passed, 0 failed, $SECONDS seconds)"
		actual="$(report_print_oneline $ctx)"
		assertEquals "$expected" "$actual"
	}

	test_shunit_report_print_oneline_all_failed() {
		local n=3
		fake_test19_failure_expected() {
			assertEquals "1" "0"
		}
		report_stats_reset $ctx
		for i in $(seq $n); do
			# Redirect stderr to /dev/null - This will make sure, when running the tests,
			# we will not see the expected/actual output that we do not care about in this test.
			runner_call_unittest $ctx "testcase_nonexistent" "fake_test19_failure_expected" 2>/dev/null
		done

		expected="FAILED ($n total, 0 passed, $n failed, $SECONDS seconds)"
		actual="$(report_print_oneline $ctx)"
		assertEquals "$expected" "$actual"
	}

	test_shunit_report_exitcode_is_one_when_at_least_one_tests_fail() {
		fake_test20_failure_expected () {
			expected="1"
			actual="0"
			assertEquals "$expected" "$actual"
		}
		# Redirect stderr to /dev/null - This will make sure, when running the tests,
		# we will not see the expected/actual output that we do not care about in this test.
		runner_call_unittest $ctx "testcase_nonexistent" "fake_test20_failure_expected" 2>/dev/null
		expected="1"
		actual="$(report_exitcode $ctx)"
		assertEquals "$expected" "$actual"
	}

	test_shunit_report_exitcode_is_zero_when_no_test_failed() {
		expected="0"
		fake_test21() {
			assertEquals "0" "0"
		}
		report_stats_reset $ctx
		runner_call_unittest $ctx "testcase_nonexistent" "fake_test21"
		actual="$(report_exitcode $ctx)"
		assertEquals "$expected" "$actual"
	}
}
