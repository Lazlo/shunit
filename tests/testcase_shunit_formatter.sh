#!/bin/bash

set -e
set -u

source libshunit.sh

testcase_shunit_formatter() {

	declare ctx

	testcase_shunit_formatter_setup() {
		runner_create_context
		ctx=$(runner_get_context)
	}
	testcase_shunit_formatter_teardown() {
		return
	}

	#
	# Unit Tests
	#

	test_shunit_formatter_opt_verbose_get() {
		expected="$__libshunit_formatter_opt_verbose"
		actual=$(formatter_opt_verbose_get $ctx)
		assertEquals "$expected" "$actual"
	}

	test_shunit_formatter_opt_verbose_set() {
		expected="42"
		formatter_opt_verbose_set $ctx $expected
		actual=$(formatter_opt_verbose_get $ctx)
		assertEquals "$expected" "$actual"
	}

	test_shunit_formatter_fail_prints_failed_when_opt_verbose_is_nonzero() {
		expected="failed!"
		formatter_opt_verbose_set $ctx 1
		actual=$(fail)
		assertEquals "$expected" "$actual"
	}

	test_shunit_formatter_pass_prints_passed_when_opt_verbose_is_nonzero() {
		expected="passed"
		formatter_opt_verbose_set $ctx 1
		actual=$(pass)
		assertEquals "$expected" "$actual"
	}

	test_shunit_formatter_fail_prints_exclamation_mark_when_opt_verbose_is_zero() {
		expected="!"
		formatter_opt_verbose_set $ctx 0
		actual="$(fail)"
		assertEquals "$expected" "$actual"
	}

	test_shunit_formatter_pass_prints_dot_when_opt_verbose_is_zero() {
		expected="."
		formatter_opt_verbose_set $ctx 0
		actual="$(pass)"
		assertEquals "$expected" "$actual"
	}

	test_shunit_formatter_colorize_truns_string_green() {
		expected="\e[32mSomeTestString\e[0m"
		actual="$(colorize "SomeTestString" "green")"
		assertEquals "$expected" "$actual"
	}

	test_shunit_formatter_colorize_turns_string_red() {
		expected="\e[31mNextTestString\e[0m"
		actual="$(colorize "NextTestString" "red")"
		assertEquals "$expected" "$actual"
	}
}
