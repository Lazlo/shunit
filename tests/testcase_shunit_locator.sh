#!/bin/bash

set -e
set -u

source libshunit.sh

testcase_shunit_locator() {
	testcase_shunit_locator_setup() {
		return
	}
	testcase_shunit_locator_teardown() {
		return
	}

	#
	# Unit Tests
	#

	test_shunit_locator_find_testcase_files() {
		expected="testcase_bar.sh testcase_foo.sh"
		local where="$tc_dir/fixtures/locator/01-testcase-files-only"
		actual=$(locator_find_testcase_files $where)
		assertEquals "$expected" "$actual"
	}

	test_shunit_locator_find_testcase_files_without_cruft() {
		expected="testcase_foobar.sh"
		local where="$tc_dir/fixtures/locator/03-directory-with-cruft-dir-and-files"
		actual=$(locator_find_testcase_files $where)
		assertEquals "$expected" "$actual"
	}

	test_shunit_locator_testcase_file_to_name() {
		expected="testcase_tank_engine"
		local tc_file="${expected}.sh"
		actual=$(__libshunit_testcase_file_to_name "$tc_file")
		assertEquals "$expected" "$actual"
	}

	test_shunit_locator_testcase_name_to_file() {
		local tc_name="testcase_boat_engine"
		expected="${tc_name}.sh"
		actual=$(__libshunit_testcase_name_to_file "$tc_name")
		assertEquals "$expected" "$actual"
	}

	test_shunit_locator_testcase_name_to_subject() {
		expected="airplane_engine"
		local tc_name="testcase_$expected"
		actual=$(__libshunit_testcase_name_to_subject "$tc_name")
		assertEquals "$expected" "$actual"
	}

	test_shunit_locator_load_testcase_loads_testcase_and_makes_nested_funcs_available() {
		expected="0"
		local testcase_name="testcase_acme_subsystem1"
		local testcase_filename="${testcase_name}.sh"
		local testcase_setup_func_name="${testcase_name}_setup"
		local where="$tc_dir/fixtures/locator/04-testcase-loading"
		local testcase_path="$where/$testcase_filename"
		locator_load_testcase $testcase_path
		# After loading the test case, the setup, tear down and test functions
		# should have been declared by the test case.
		# Here, we only check for the setup function being declared.
		set +e
		declare -F | grep "${testcase_setup_func_name}" > /dev/null
		actual=$?
		set -e
		assertEquals "$expected" "$actual"
	}

	test_shunit_locator_testcase_has_setup_returns_one_when_tc_has_no_setup() {
		expected="1"
		local testcase_name="testcase_acme_nosetup"
		local testcase_filename="${testcase_name}.sh"
		local testcase_setup_func_name="${testcase_name}_setup"
		local where="$tc_dir/fixtures/locator/05-testcase-setup-teardown"
		local testcase_path="$where/$testcase_filename"
		locator_load_testcase $testcase_path
		actual=$(locator_testcase_has_setup $testcase_name)
		assertEquals "$expected" "$actual"
	}

	test_shunit_locator_testcase_has_setup_returns_zero_when_tc_has_setup() {
		expected="0"
		local testcase_name="testcase_acme_withsetup"
		local testcase_filename="${testcase_name}.sh"
		local testcase_setup_func_name="${testcase_name}_setup"
		local where="$tc_dir/fixtures/locator/05-testcase-setup-teardown"
		local testcase_path="$where/$testcase_filename"
		locator_load_testcase $testcase_path
		actual=$(locator_testcase_has_setup $testcase_name)
		assertEquals "$expected" "$actual"
	}

	test_shunit_locator_testcase_has_teardown_returns_one_when_tc_has_no_teardown() {
		expected="1"
		local testcase_name="testcase_acme_noteardown"
		local testcase_filename="${testcase_name}.sh"
		local testcase_teardown_func_name="${testcase_name}_teardown"
		local where="$tc_dir/fixtures/locator/05-testcase-setup-teardown"
		local testcase_path="$where/$testcase_filename"
		locator_load_testcase $testcase_path
		actual=$(locator_testcase_has_teardown $testcase_name)
		assertEquals "$expected" "$actual"
	}

	test_shunit_locator_testcase_has_teardown_returns_zero_when_tc_has_teardown() {
		expected="0"
		local testcase_name="testcase_acme_withteardown"
		local testcase_filename="${testcase_name}.sh"
		local testcase_teardown_func_name="${testcase_name}_teardown"
		local where="$tc_dir/fixtures/locator/05-testcase-setup-teardown"
		local testcase_path="$where/$testcase_filename"
		locator_load_testcase $testcase_path
		actual=$(locator_testcase_has_teardown $testcase_name)
		assertEquals "$expected" "$actual"
	}

	test_shunit_locator_find_unittests_by_testcase_name() {
		expected="test_acme_say_goodbye test_acme_say_hello"
		local where="$tc_dir/fixtures/locator/02-testcase-with-unittests"
		actual=$(locator_find_unittests_by_testcase_name $where "testcase_acme")
		assertEquals "$expected" "$actual"
	}
}
