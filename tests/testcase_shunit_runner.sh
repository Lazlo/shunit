#!/bin/bash

set -e
set -u

source libshunit.sh

testcase_shunit_runner() {

	declare ctx

	testcase_shunit_runner_setup() {
		runner_create_context
		ctx=$(runner_get_context)
	}
	testcase_shunit_runner_teardown() {
		return
	}

	#
	# Unit Tests
	#

	test_shunit_runner_get_last_context_returns_last_created_context_id() {
		runner_create_context
		actual=$(runner_get_context)
		local -r -i i_last=$((${#__libshunit_context_list[@]} - 1))
		expected="${__libshunit_context_list[$i_last]}"
		assertEquals "$expected" "$actual"
	}

	test_shunit_runner_create_context_retruns_random_id() {
		local call_n_times=10

		# Create N context ids and save them in a list

		local -a ctx_list
		for n in $(seq 0 $(($call_n_times - 1))); do
			runner_create_context
			ctx_list[$n]="$(runner_get_context)"
		done

		# Then perform a statistical operation,
		# counting the unique context ids.

		# Get length of the list
		local ctx_list_len=${#ctx_list[@]}
		local -a ctx_stats
		local curr_ctx
		for i in $(seq 0 $(($ctx_list_len - 1))); do
			# Get each context id
			curr_ctx="${ctx_list[$i]}"
			# Increment the occurrence count for the context
			if [ $(echo "${!ctx_stats[@]}" | grep "^$curr_ctx$") ]; then
				ctx_stats[$curr_ctx]=$((${ctx_stats[$curr_ctx]} + 1))
			else
				ctx_stats[$curr_ctx]=1
			fi
		done

		# Now check, that the context list is as long as the statistics list.
		# This implies all context ids are unique. Otherwise, the statistics
		# list would have been shorter.

		expected=$ctx_list_len
		actual=${#ctx_stats[@]}

		assertEquals "$expected" "$actual"
	}

	test_shunit_runner_call_testcase_setup() {
		expected="called"
		ooo_setup_called="not called"
		testcase_ooo() {
			testcase_ooo_setup() {
				ooo_setup_called="called"
			}
		}
		# Make nested functions of testcase available
		testcase_ooo
		runner_call_testcase_setup $ctx "testcase_ooo"
		actual="$ooo_setup_called"
		assertEquals "$expected" "$actual"
	}

	test_shunit_runner_call_testcase_teardown() {
		expected="called"
		ggg_teardown_called="not called"
		testcase_ggg() {
			testcase_ggg_teardown() {
				ggg_teardown_called="called"
			}
		}
		# Make nested functions of testcase available
		testcase_ggg
		runner_call_testcase_teardown $ctx "testcase_ggg"
		actual="$ggg_teardown_called"
		assertEquals "$expected" "$actual"
	}

	test_shunit_runner_call_unittest_increments_stats_tests_total() {
		fake_test1() {
			expected="12"
			actual="12"
			assertEquals "$expected" "$actual"
		}
		report_stats_reset $ctx
		runner_call_unittest $ctx "testcase_dontcare" "fake_test1"
		runner_call_unittest $ctx "testcase_dontcare" "fake_test1"
		runner_call_unittest $ctx "testcase_dontcare" "fake_test1"
		# Make sure to declare our real expected and actual only after
		# the fake test has been executed.
		expected="3"
		actual=$(report_stats_tests_total_get $ctx)
		assertEquals "$expected" "$actual"
	}

	test_shunit_runner_call_unittest_increments_stats_passed() {
		expected="2"
		fake_test_foobar23() {
			assertEquals "0" "0"
		}
		report_stats_reset $ctx
		runner_call_unittest $ctx "testcase_dontcare" "fake_test_foobar23"
		runner_call_unittest $ctx "testcase_dontcare" "fake_test_foobar23"
		actual=$(report_stats_tests_passed_get $ctx)
		assertEquals "$expected" "$actual"
	}

	test_shunit_runner_call_unittest_increments_stats_failed() {
		fake_test42_failure_expected() {
			expected="1"
			actual="0"
			assertEquals "$expected" "$actual"
		}
		report_stats_reset $ctx
		# Redirect stderr to /dev/null - This will make sure, when running the tests,
		# we will not see the expected/actual output that we do not care about in this test.
		runner_call_unittest $ctx "testcase_dontcare" "fake_test42_failure_expected" 2>/dev/null
		runner_call_unittest $ctx "testcase_dontcare" "fake_test42_failure_expected" 2>/dev/null
		runner_call_unittest $ctx "testcase_dontcare" "fake_test42_failure_expected" 2>/dev/null
		expected="3"
		actual=$(report_stats_tests_failed_get $ctx)
		assertEquals "$expected" "$actual"
	}

	# TODO Re-write this test, so it will work with the
	# new behavior of runnre_call_unittest.
	#
	# NOTE As the unittest is executed in a subshell by
	# runner_call_unittest, this test will no longer work.
	# This is because, from the subshell, the unittest
	# will not be able to write to the magic_var of the
	# invoking shell.
	#test_shunit_runner_call_unittest_calls_function() {
	#	magic_var="not called"
	#	fake_test2() {
	#		magic_var="called"
	#	}
	#	expected="called"
	#	runner_call_unittest $ctx "testcase_dontcare" "fake_test2"
	#	actual="$magic_var"
	#	assertEquals "$expected" "$actual"
	#}

	test_shunit_runner_call_unittest_calls_testcase_setup() {
		setup_called="not called"
		testcase_xxx() {
			testcase_xxx_setup() {
				setup_called="called"
				return
			}
			test_xxx_sometest() {
				assertEquals "52" "52"
			}
		}
		# Make nested functions of testcase available
		testcase_xxx
		runner_call_unittest $ctx "testcase_xxx" "test_xxx_sometest"
		expected="called"
		actual="$setup_called"
		assertEquals "$expected" "$actual"
	}

	test_shunit_runner_call_unittest_calls_testcase_teardown() {
		teardown_called="not called"
		testcase_yyy() {
			testcase_yyy_teardown() {
				teardown_called="called"
				return
			}
			test_yyy_sometest() {
				assertEquals "71" "71"
			}
		}
		# Make nested functions of testcase available
		testcase_yyy
		runner_call_unittest $ctx "testcase_yyy" "test_yyy_sometest"
		expected="called"
		actual="$teardown_called"
		assertEquals "$expected" "$actual"
	}

	test_shunit_runner_call_unittest_prints_warning_to_stderr_when_rc_var_not_set() {
		test_warn_because_no_rc_set() {
			return
		}
		expected="$(echo -e "\nWARNING: ${FUNCNAME}() did not set rc! Make sure the test calls assertEquals().")"
		actual="$(runner_call_unittest $ctx "testcase_nonexistent" "test_warn_because_no_rc_set" 2>&1)"
		assertEquals "$expected" "$actual"
	}

	# TODO Implement context base unittest outcome, before we activate this test.
	# As of now, this test will always fail because the unittests of shunit itself
	# will set the outcome without respect to context.
	#test_shunit_runner_get_unittest_outcome_returns_invalid_when_no_test_was_executed() {
	#	expected="invalid"
	#	actual="$(runner_get_unittest_outcome $ctx)"
	#	assertEquals "$expected" "$actual"
	#}

	test_shunit_runner_get_unittest_outcome() {
		testcase_check_outcome() {
			test_check_outcome_failure_expected() {
				expected="5"
				actual="7"
				assertEquals "$expected" "$actual"
			}
		}
		# Make nested functions of testcase available
		testcase_check_outcome
		# Redirect stderr to /dev/null - This will make sure, when running the tests,
		# we will not see the expected/actual output that we do not care about in this test.
		runner_call_unittest $ctx "testcase_check_outcome" "test_check_outcome_failure_expected" 2>/dev/null
		expected="1"
		actual="$(runner_get_unittest_outcome $ctx)"
		assertEquals "$expected" "$actual"
	}
}
