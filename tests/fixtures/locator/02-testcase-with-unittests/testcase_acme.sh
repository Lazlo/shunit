set -e
set -u

source libshunit.sh

test_acme_say_hello() {
	echo "HELLO"
}

test_acme_say_goodbye() {
	echo "GOODBYE"
}
